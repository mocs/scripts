#!/usr/bin/env bash

# This file is used to define user interaction functions

function format_time(){
  # No debug here
  set +x
  # Check that inputs are valid numbers
  re='^[+-]?[0-9]+([.][0-9]+)?$'
  if ! [[ "$1" =~ $re ]]; then
    echo "N/A"
    return
  fi
  local IFS='.'
  read -a data <<< "${1}"
  local sec=${data[0]}
  local frac=${data[1]}
  if [[ $sec -ge 86400 ]];then
    local days=$(( $sec / 86400 ))
    printf "${days} d "
    local diff=$(( $days * 86400 ))
    local sec=$(( $sec - $diff ))
    local doPrint=1
  fi
  if [[ $sec -ge 3600 ]] || [[ $doPrint == 1 ]];then
    local hours=$(( $sec / 3600 ))
    printf "${hours} h "
    local diff=$(( $hours * 3600 ))
    local sec=$(( $sec - $diff ))
    local doPrint=1
  fi
  if [[ $sec -ge 60 ]] || [[ $doPrint == 1 ]];then
    local min=$(( $sec / 60 ))
    printf "${min} m "
    local diff=$(( $min * 60 ))
    local sec=$(( $sec - $diff ))
  fi
  printf "${sec} s "
  printf "${frac} ms"
  # Restore DEBUG
  if ! [ -z ${DEBUG+x} ]; then
    set -x
  fi
}


# Function to get the length of a string _without_ color codes.
function string_length() {
  local _re_colors='(\\033\[[0-9]{1,2};?([0-9]{1,2})?m)'
  local _left=${1}
  local _len=${#_left}
  while [[ "$_left" =~ $_re_colors ]]; do
    _left=${_left#*$BASH_REMATCH}
    _len=$(($_len-${#BASH_REMATCH}))
  done
  printf "$_len"
}

# Function to split text into lines. To allow empty lines, § is used to
# indicate empty lines.
function split_text() {
  # Check input. Priority for width:
  # 1. Second argument
  # 2. LINE_WIDTH
  # 3. 80
  if [[ $# -ne 2 ]]; then
    if [[ -z $LINE_WIDTH ]]; then
      local LINE_WIDTH=80
    fi
  else
    local LINE_WIDTH=$2
  fi
  local _line=""
  local _word
  for _word in $1; do
    if [[ "${_word}" = "§" ]]; then
      if [[ ${#_line} -eq 0 ]]; then
        echo " "
      else
        echo "$_line"
      fi
      local _line=""
    else
      if [[ ${#_line} -eq 0 ]]; then
        local _tmp="$_word"
      else
        local _tmp="$_line $_word"
      fi
      if [[ $(string_length "$_tmp") -gt $LINE_WIDTH ]]; then
        echo "$_line"
        # Check for superlong words...
        while [[ $(string_length "$_word") -gt $LINE_WIDTH ]]; do
          echo "${_word:0:$LINE_WIDTH}"
          local _word="${_word:$LINE_WIDTH}"
        done
        local _line="$_word"
      else
        local _line="$_tmp"
      fi
    fi
  done
  if [[ ${#_line} -gt 0 ]]; then
    echo "$_line"
  fi
}

# Message depending on screen width
function message() {
  # No debug here
  set +x
  local _info
  local _left
  local _base_color
  local _reset_color
  local _reset_color_last
  local _re_color
  _re_color='\\033\[[0-1]\;3[0-7]m'
  if [[ $# -gt 1 ]]; then
    _base_color="$2"
  fi
  if [[ -z $LINE_WIDTH ]]; then
    local LINE_WIDTH=80
  fi
  local _fill=$(printf "%${LINE_WIDTH}s")
  printf "${_fill// /#}\n"
  local _lines=$(split_text "$1" $(($LINE_WIDTH-6)))
  IFS=$'\r\n' _lines=($(echo "${_lines}"))
  unset IFS
  local _line
  for _line in "${_lines[@]}"; do
    _info=$(string_length "$_line")
    _left=$((${LINE_WIDTH}-5-$_info))
    local _sfill=$(printf "%${_left}s")
    # Get _reset_color
    if [[ "${_line}" =~ $_re_color ]]; then
      _reset_color=${BASH_REMATCH[@]}
    fi
    printf "##${_reset_color_last} $_line${_sfill// / }${_base_color}##\n"
    # Remember color
    _reset_color_last=${_reset_color}
  done
  printf "${_fill// /#}\n"
  # Restore DEBUG
  if ! [ -z ${DEBUG+x} ]; then
    set -x
  fi
}

# Function to show a progressbar
function ProgressBar() {
  # No debug here
  set +x
  if [[ -z $LINE_WIDTH ]]; then
    local LINE_WIDTH=80
  fi
  local _part=$1
  local _total=$2
  local _left
  local _right
  if [[ $_total -eq 0 ]]; then
    local _message="Cannot show progressbar. Total is 0. ($_part/$_total)"
  elif [[ $_total -lt $_part ]]; then
    local _message="Cannot show progressbar. Progess is too large. ($_part/$_total)"
  fi
  if ! [[ -z $_message ]]; then
    local _mess_len=${#_message}
    _left=$(($(($LINE_WIDTH-9-$_mess_len))/2))
    _right=$(($LINE_WIDTH-9-$_left-$_mess_len))
    local _fill=$(printf "%${_left}s")
    local _empty=$(printf "%${_right}s")
    printf "\r${YELLOW}[${_fill// / }${red}${_message}${YELLOW}${_empty// / }]   #N/A"
    return
  fi
  # All good, continue.
  local _prog=$(percentage "${_part}" "${_total}")
  local _progress
  local _done
  local _pleft
  _progress=$(($((${_part}*100/${_total}*100))/100))
  _done=$(($((${_progress}*$(($LINE_WIDTH-9))))/100))
  _left=$(($LINE_WIDTH-9-$_done))
  _pleft=$((6-${#_prog}))
  local _fill=$(printf "%${_done}s")
  local _empty=$(printf "%${_left}s")
  local _pempty=$(printf "%${_pleft}s")
  printf "\r${YELLOW}[${_fill// /#}${_empty// /-}]${_pempty// / }${_prog}%%"
  # Restore DEBUG
  if ! [ -z ${DEBUG+x} ]; then
    set -x
  fi
}

# Pretty print large numbers
function num_print() {
  # No debug here
  set +x
  # Simply add thousand separators.
  local _data=($(echo "$1" | awk -F'.' '{print $1;print $2}'))
  local _dec=${_data[1]}
  local _data=${_data[0]}
  local _result=""
  while [[ ${#_data} -gt 3 ]]; do
    local _pos=$((${#_data}-3))
    local _end=${_data:$_pos:3}
    local _data=${_data%???}
    if [[ ${#_result} -eq 0 ]]; then
      local _result="$_end"
    else
      local _result="$_end,$_result"
    fi
  done
  if [[ ${#_dec} -eq 0 ]]; then
    printf "$_data,$_result"
  else
    printf "$_data,$_result.$_dec"
  fi
  # Restore DEBUG
  if ! [ -z ${DEBUG+x} ]; then
    set -x
  fi
}
