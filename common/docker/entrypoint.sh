#!/usr/bin/env bash

# This file acts as an entrypoint for the container.
source /bin/shell.sh

# Save arguments as CMD
CMD=$@

# Create exit hook
exit_hook() {
  printf "${RED}"
  printf "Did you save your data to repo?\n"
  printf "Any non-saved data will disappear!\n"
  printf "${CYAN}"
  printf "\nDo you want to exit?(y/n)"
  read text
  if [ "$text" == "y" ]; then
    printf "Bye.\n\n"
    printf "${NC}"
  else
    printf "Ok, please continue.\n"
    printf "${NC}"
    entrypoint.sh $CMD
  fi
}

# If BATCH is not set, catch exit!
if [ -z "$BATCH" ];then
  # Sanity check, if CMD is empty, don't trap exit since it's no point!
  if [ ${#CMD} -eq 0 ]; then
    printf "${CYAN}"
    printf "CMD is unset. Will cause infinite exit loop.\n"
    printf "Will ${RED}not${CYAN} set exit trap.\n${NC}"
    export NOWARN=1
  else
    trap exit_hook EXIT
  fi
fi

# Info to user if BATCH is not set.
if [ -z "$BATCH" ] && [ -z "$NOWARN" ]; then
  source /bin/warning.txt
  export NOWARN=1
fi

# Execute CMD
eval $CMD
