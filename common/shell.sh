#!/usr/bin/env bash

# This file is used to define all kinds of stuff used in the shell scripts.

# Get the folder name of _this_ script
_SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
source ${_SRCDIR}/color.sh
source ${_SRCDIR}/math.sh
source ${_SRCDIR}/user_interaction.sh

# Check for success
function check_success() {
  if [[ $1 != 0 ]];then
    printf "${RED}"
    printf "Build was unsuccessful.\n"
    printf "${red}"
    printf "${RED}"
    printf "Exiting.\n"
    printf "${NC}"
    exit ${1}
  fi
}

# Define an exit command
function _exit_cmd() {
  printf "${RED}Fail\n"
  printf "${BLUE}${1}\n"
  printf "${red}Exiting.${NC}\n"
  exit ${2}
}

# Trap exit codes
trap exit_handler EXIT
function exit_handler() {
  code=$?
  # No debug here
  set +x
  if [[ $code -ne 0 ]]; then
    printf "\n${RED}"
    build_message="Execution failed! Please review any error messages!"
    if ! [[ -z $ERR ]]; then
      build_message="$build_message § ${BLUE}${ERR[@]}${RED}"
    fi
    message "$build_message"
    printf "${NC}"
  fi
  exit $code
}

# Function to generate random passwords
function random_string() {
  local _len
  if ! [[ -z $1 ]];then
    _len=$1
  else
    _len=32
  fi
  result=$(openssl rand -base64 $(($_len*2)) | tr -dc _A-Z-a-z-0-9 | head -c${1:-$_len})
  echo "$result"
}

# Function to get the local IP address
function get_local_ip() {
  # Get a list of all interfaces using "BROADCAST"
  local _ifList=()
  IFS=$'\r\n' _ifList=($(ifconfig | grep -E '^[a-zA-Z]{2,9}[0-9]{1,2}(.)*BROADCAST(.)*' | awk -F':' '{print $1}'))
  unset IFS
  # Loop over them and check for "media"
  local _if
  local _result
  for _if in "${_ifList[@]}"; do
    # For Mac OS X
    _result=$(ifconfig ${_if} | grep "media:" )
    if [[ ${#_result} -gt 0 ]]; then
      _result=$(ifconfig ${_if} | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
      echo "$_result"
      return
    fi
    # For linux
    _result=$(ifconfig ${_if} | grep "device interrupt" )
    if [[ ${#_result} -gt 0 ]]; then
      _result=$(ifconfig ${_if} | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
      echo "$_result"
      return
    fi
  done
  # Exit with failure!
  echo "0.0.0.0"
  exit 2
}
