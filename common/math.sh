#!/usr/bin/env bash

# This file is used to define math functions

# Function for addition using awk
function add() {
  local _final=$(awk -v v1="$1" -v v2="$2" -v OFMT="%.3f" 'BEGIN {print v1+v2}')
  echo ${_final}
}

# Function for subtraction using awk
function subtract() {
  local _final=$(awk -v v1="$1" -v v2="$2" -v OFMT="%.3f" 'BEGIN {print v1-v2}')
  echo ${_final}
}

# Function for multiplication using awk
function multiply() {
  if [ $# -lt 3 ]; then
    local _format="%.3f"
  else
    local _format="$3"
  fi
  local _final=$(awk -v v1="$1" -v v2="$2" -v OFMT="$_format" 'BEGIN {print v1*v2}')
  echo ${_final}
}

# Function for division using awk
function divide() {
  if [ $# -lt 3 ]; then
    local _format="%.3f"
  else
    local _format="$3"
  fi
  local _final=$(awk -v v1="$1" -v v2="$2" -v OFMT="$_format" 'BEGIN {print v1/v2}')
  echo ${_final}
}

# Function for percentage using awk
function percentage() {
  # Input checking. Swap , for .
  nom=$(echo "$1" | sed 's/,/./g')
  denom=$(echo "$2" | sed 's/,/./g')
  # Check that inputs are valid numbers
  re='^[+-]?[0-9]+([.][0-9]+)?$'
  if ! [[ "$nom" =~ $re ]] || ! [[ "$denom" =~ $re ]]; then
    echo "N/A"
    return
  fi
  # Catch division by zero
  #re_zero='^[+-]?[0]+([.][0]{0,15}([1-9]+[0-9]*)?)?$'
  re_zero='^[+-]?[0]+(([.]([0]{16,}([1-9]+[0-9]*)?))|([.]([0]{0,15})?)?)$'
  if [[ $denom =~ $re_zero ]];then
    echo "NaN"
    return
  fi
  local _final=$(awk -v v1=$nom -v v2=$denom -v OFMT="%.2f" 'BEGIN {print v1*100/v2}')
  if [[ $_final != *"."* ]]; then
    local _final="${_final}.00"
  fi
  echo ${_final}
}
