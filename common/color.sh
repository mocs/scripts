#!/usr/bin/env bash
# This file contains the color codes.

# Define all colors
BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
WHITE='\033[0;37m'

black='\033[1;30m'
red='\033[1;31m'
green='\033[1;32m'
yellow='\033[1;33m'
blue='\033[1;34m'
magenta='\033[1;35m'
cyan='\033[1;36m'
white='\033[1;37m'

NC='\033[0m'

# Also define all colors as functions
function BLACK(){
  echo "\033[0;30m$1\033[0m"
}

function RED(){
  echo "\033[0;31m$1\033[0m"
}

function GREEN(){
  echo "\033[0;32m$1\033[0m"
}

function YELLOW(){
  echo "\033[0;33m$1\033[0m"
}

function BLUE(){
  echo "\033[0;34m$1\033[0m"
}

function MAGENTA(){
  echo "\033[0;35m$1\033[0m"
}

function CYAN(){
  echo "\033[0;36m$1\033[0m"
}

function WHITE(){
  echo "\033[0;37m$1\033[0m"
}

function black(){
  echo "\033[1;30m$1\033[0m"
}

function red(){
  echo "\033[1;31m$1\033[0m"
}

function green(){
  echo "\033[1;32m$1\033[0m"
}

function yellow(){
  echo "\033[1;33m$1\033[0m"
}

function blue(){
  echo "\033[1;34m$1\033[0m"
}

function magenta(){
  echo "\033[1;35m$1\033[0m"
}

function cyan(){
  echo "\033[1;36m$1\033[0m"
}

function white(){
  echo "\033[1;37m$1\033[0m"
}
