# Common code

Common code that is used everywhere as a submodule.

## common
The _common_ folder contains bash scripts that can be included by sourcing.

## bin
The _bin_ folder contains stand-alone binaries (often in the form of bash scripts).
