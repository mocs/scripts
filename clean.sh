#!/usr/bin/env bash

# Use this file to start the build.

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

# Get common stuff
source ${SRCDIR}/common/shell.sh
printf "${CYAN}"
printf "Using manifest file: ${BLUE}${MANIFEST}${NC}\n"

# Sanity checks
printf "${MAGENTA}"
message "Executing sanity checks!"
printf "${NC}"
text="${cyan}Checking that we have a ${MANIFEST}..."
printf "     $text"
if ! [[ -f ${MANIFEST} ]]; then
  printf "\r"
  _exit_cmd "Could not find MANIFEST in ${TARGET}." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
source ${MANIFEST}

# Cleanup!
printf "${MAGENTA}"
message "Cleaning!"
printf "${NC}"
printf "${CYAN}"
printf "Cleaning...\n"
printf "${cyan}"
START=$(date +%s.%N | tr 'N' '0')
IFS=$'\r\n' ids=($(${docker_cmd} ps --filter "status=exited" --filter "status=created" -q | awk '{print $1}'))
unset IFS
printf "${cyan}"
for id in "${ids[@]}"; do
  message "Removing stopped container ${blue}${id}${cyan}
  $(${docker_cmd} rm ${id} 2>&1)" "${cyan}"
done
printf "${NC}"
sleep 3
IFS=$'\r\n' ids=($(${docker_cmd} images -f dangling=true -q | awk '{print $1}' ))
unset IFS
printf "${cyan}"
for id in "${ids[@]}"; do
  message "Removing container image ${blue}${id}${cyan}
  $(${docker_cmd} rmi -f ${id} 2>&1)" "${cyan}"
done
printf "${NC}"
STOP=$(date +%s.%N | tr 'N' '0')
DIFF=$(subtract "$STOP" "$START")
TIME=$(format_time "${DIFF}")
printf "${CYAN}"
printf "Done cleaning in ${TIME}.\n\n"

#Exit
printf "${MAGENTA}"
message "All done!"
printf "${NC}"
