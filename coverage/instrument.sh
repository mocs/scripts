#!/usr/bin/env bash

# This file is used to instrument bash script to allow coverage
# measurments during bats testing.

# Get the folder name of _this_ script
_SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
source ${_SRCDIR}/../common/shell.sh

# Info
usage="${CYAN}"${0##*/}" [-h] file ${GREEN}-- program to instrument a bash file.

where:
  ${blue}-h${GREEN}   show this help text
  ${blue}file${GREEN} file to instrument"

# Check arguments
while getopts ':hs:' option; do
  case "$option" in
  h)
    printf "$usage"
    exit
    ;;
  \?)
    printf "${RED}illegal option: -%s\n" "$OPTARG" >&2
    printf "$usage" >&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))
if [[ "$#" -ge 1 ]]; then
  # Check that it is a valid bash file
  if ! [ -f "$1" ]; then
    printf "${RED}Illegal bash file given.${NC}"
    exit 2
  else
    inFile="$1"
  fi
else
  printf "${RED}Missing file!${NC}"
  exit 3
fi

# Now, let's keep it simple. Loop over each line and create a new bash-file that has
# the added functionality of writing calls to a coverage file.
out="$inFile.instr"
cfg="$inFile.cov.cfg"
raw="$inFile.cov.raw"
touch $raw
i=1

while read -r p || [ -n "$p" ]
do
  # Skip empty and commented lines. Also skip } and { and ifs
  if [ -z "$p" ] || [[ "$p" =~ ^#.*$ ]] || [[ "$p" =~ ^function.*$ ]] || [[ "$p" =~ ^}$ ]] || [[ "$p" =~ ^fi$ ]] || [[ "$p" =~ ^else$ ]] || [[ "$p" =~ ^(while)|(for).*do$ ]] || [[ "$p" =~ case.*in$ ]] || [[ "$p" =~ .*\)$ ]] || [[ "$p" =~ \;\;$ ]]; then
    printf "%s\n" "$p" >> "$out"
  else
    # Instrument the line.
    # BASH_SOURCE makes sure we can ignore false hits from sourcing.
    # We must skip the && for certain commands
    if [[ "$p" =~ ^function.*$ ]] || [[ "$p" =~ ^.*then$ ]];then
      printf "%s echo $i \$BASH_SOURCE >> $raw\n" "$p" >> "$out"
    elif [[ "$p" =~ ^return.*$ ]] || [[ "$p" =~ ^exit.*$ ]]; then # For some commands the instrumentation must come before the command
      printf "echo $i \$BASH_SOURCE >> $raw && %s\n" "$p" >> "$out"
    else
      printf "%s && echo $i \$BASH_SOURCE >> $raw\n" "$p" >> "$out"
    fi
    # And keep track of which lines are instrumented
    echo $i >> "$cfg"
  fi
  # Keep count
  i=$(( i+=1 ))
done < "$inFile"

# Now replace the original file with the instrumented.
rm "${inFile}"
mv "${out}" "${inFile}"
