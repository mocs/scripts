#!/usr/bin/env bash

# Function for pretty printing coverage results
function coverage_table(){
  local len=$1
  local data=$(printf $2)

  while read -r line; do
    local info=($(echo "$line" | sed 's/##/ /g'))
    local covInt=$(printf %.0f "${info[1]}")
    local mess_len="${#info[0]}"
    let _left=$len-$mess_len+2 || true
    let _pleft=6-${#info[1]} || true
    local _empty=$(printf "%${_left}s")
    local _pempty=$(printf "%${_pleft}s")
    if [[ "${info[0]}" == "SubTotal" ]]; then
      printf "${magenta}${info[0]}${_empty// /_}${_pempty// /_}"
    elif [[ "${info[0]}" == "Total" ]]; then
      printf "${green}${info[0]}${_empty// /_}${_pempty// /_}"
    else
      printf "${blue}${info[0]}${_empty// / }${_pempty// / }"
    fi
    if [[ "${covInt}" -eq 10000 ]]; then
      printf "${magenta}"
    elif [[ "${covInt}" -eq 10001 ]]; then
      printf "${green}"
    elif [[ "${covInt}" -gt 99 ]]; then
      printf "${green}"
    elif [[ ${covInt} -ge 75 ]]; then
      printf "${yellow}"
    else
      printf "${red}"
    fi
    if [[ "${covInt}" -ge 10000 ]]; then
      printf "______"
    else
      printf "${info[1]}"
      if [[ ${#info[0]} -gt 0 ]]; then
        printf "%%"
      fi
    fi
    printf "${NC}\n"
  done <<< "$data"
}

# Function for sorting array of file names on depth first
function depth_sort() {
  # Simply count the number of / in each item and sort accordingly.
  local _input
  declare -a _input=("${!1}")
  local _count=${#_input[@]}
  local _maxDepth=0
  # Get the _maxDepth
  for item in "${_input[@]}"; do
    local _depth=$(echo "$item" | tr -d -c '/\n' | awk '{ print length; }')
    if [ "$_depth" -gt "$_maxDepth" ]; then
      _maxDepth=$_depth
    fi
  done
  # Loop until all items have been sorted
  while [[ $_count -ne 0 ]]; do
    # Find all items with current depth
    for item in "${_input[@]}"; do
      local _depth=$(echo "$item" | tr -d -c '/\n' | awk '{ print length; }')
      if [ "$_depth" -eq "$_maxDepth" ]; then
        _count=$(($_count-1))
        echo "$item"
      fi
    done
    _maxDepth=$(($_maxDepth-1))
  done
}
