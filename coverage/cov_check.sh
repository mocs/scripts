#!/usr/bin/env bash

# This file displays any rows that is missing test coverage. It requires
# .cov files to be present, i.e., test suite with coverage must have been
# executed.

# Get the folder name of _this_ script
_SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
source ${_SRCDIR}/../common/shell.sh

# Info
usage="${CYAN}"${0##*/}" [-h] folder ${GREEN}-- program to display rows that are missing tests.

where:
  ${blue}-h${GREEN}     show this help text
  ${blue}folder${GREEN} folder or file to analyze"

# Check arguments
doCov=0
while getopts ':hs:' option; do
  case "$option" in
  h)
    printf "$usage"
    exit
    ;;
  \?)
    printf "${RED}illegal option: -%s\n" "$OPTARG" >&2
    printf "$usage" >&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))
if [[ "$#" -ge 1 ]]; then
  # Check that it is a valid folder
  if ! [ -d "$1" ]; then
    if ! [ -f "$1" ]; then
      printf "${RED}Illegal folder or file given.${NC}"
      exit 2
    else
      file="${1##*/}"
      folder="${1%/*}"
    fi
  else
    file="*.sh"
    folder="$1"
  fi
else
  printf "${RED}Missing folder or file!${NC}"
  exit 3
fi
# Change to folder
tool=$(pwd)
cd ${folder}

# Setup regexps
rRow='("[^"]*")'
rHit='([0-9]*,)'

# Find all files
for filename in $file; do
  printf $(cyan "Analyzing")
  printf " $(blue $filename)"
  printf "$(cyan ...)\n"
  # Check that cov file exits.
  covFile="${filename}.cov"
  if ! [[ -f "${covFile}" ]]; then
    printf "$(red 'No coverage file found!')\n"
    continue
  fi
  rowsPrint=()
  rowsZero=()
  # Read the coverage file and find any zeros.
  while read -r p || [ -n "$p" ]
  do
    # Split data to find zeros
    row=$([[ "$p" =~ $rRow ]] && echo ${BASH_REMATCH[1]})
    row="${row%\"}"
    row="${row#\"}"
    hit=$([[ "$p" =~ $rHit ]] && echo ${BASH_REMATCH[1]})
    hit="${hit%,}"
    if [[ "${hit}" = "0" ]]; then
      rowsZero[${row}]=1
      # Print +/- 3 rows!
      for i in $(seq $(( ${row} - 3 )) $(( ${row} + 3 ))); do
        rowsPrint[$i]=1
      done
    fi
  done < "$covFile"
  if [[ ${#rowsPrint[@]} -eq 0 ]]; then
    printf "$(green 'Coverage is complete!')\n"
    continue
  fi
  for i in "${!rowsPrint[@]}"; do
    if [[ ${rowsZero[$i]} -eq 1 ]]; then
      printf "${red}--> "
    else
      printf "${green}    "
    fi
    printf "%s\t" "$i"
    sed -n ${i}p ${filename}
  done
  printf "\n"
done


cd ${tool}
