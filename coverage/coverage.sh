#!/usr/bin/env bash

# This file is used to calculate the coverage from running bats tests on
# instrumented scripts.

# Get the folder name of _this_ script
_SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
source ${_SRCDIR}/../common/shell.sh

# Info
usage="${CYAN}"${0##*/}" [-h] file folder${GREEN}-- program to calculate coverage from instrumented bash files

where:
  ${blue}-h${GREEN}     show this help text
  ${blue}file${GREEN}   file to calculate coverage on
  ${blue}folder${GREEN} folder to save all.cov"

# Check arguments
while getopts ':hs:' option; do
  case "$option" in
  h)
    printf "$usage"
    exit
    ;;
  \?)
    printf "${RED}illegal option: -%s\n" "$OPTARG" >&2
    printf "$usage" >&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))
if [[ "$#" -ge 1 ]]; then
  # Check that it is a valid bash file
  if ! [ -f "$1" ]; then
    printf "${RED}Illegal bash file given.${NC}"
    exit 2
  else
    inFile="$1"
  fi
else
  printf "${RED}Missing file!${NC}"
  exit 3
fi
if [[ "$#" -ge 2 ]]; then
  # Check that it is a valid folder
  if ! [ -d "$2" ]; then
    echo "$2"
    printf "${RED}Illegal folder given.${NC}"
    exit 2
  else
    allFolder="$2"
  fi
else
  printf "${RED}Missing folder!${NC}"
  exit 3
fi

# Now, let's keep it simple. Loop over each line and create a new bash-file that has
# the added functionality of writing calls to a coverage file.
out="$inFile"
cfg="$out.cov.cfg"
raw="$out.cov.raw"
out="$inFile.cov"
outFile=$(echo "$inFile" | awk -F${allFolder}/src/ '{print $2}')

# First check number of lines
nol=$(cat $inFile | wc -l)
# Initialize array, -1 means don't count.
data=()
for i in $(seq 1 $nol);do
  data[$i]=-1
done

# Now read config file to find which lines should be checked
while read -r p || [ -n "$p" ]
do
  data[$p]=0
done < "$cfg"

# Now check the raw data and store in array.
while read -r p || [ -n "$p" ]
do
  # Split into array by space
  d=($p)
  data[${d[0]}]=$(( data[${d[0]}]+=1 ))
done < "$raw"

# Create an output json coverage file
echo "{" > $out
echo "  \"coverage\": {" >> $out
echo "    \"$inFile\": {" >> $out
i=1
for d in ${data[@]};do
  if [[ $d -lt 0 ]];then
    echo "      \"$i\": null," >> $out
  else
    echo "      \"$i\": $d," >> $out
  fi
  i=$(( i+=1 ))
done
echo "    }" >> $out
echo "  }" >> $out
echo "}" >> $out

# Calculate total coverage
loc=0
hits=0
for d in ${!data[@]};do
  if [[ ${data[$d]} -lt 0 ]]; then
    loc=$(($loc+1))
  elif [[ ${data[$d]} -ne 0 ]]; then
    hits=$(($hits+1))
  fi
done
loc=$(($nol-$loc))
cov=$(percentage $hits $loc)
echo "$outFile $hits $loc" >> "${allFolder}/all.cov"
