#!/usr/bin/env bash

# Use this file to execute a test suite.

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

# Get common stuff
source ${SRCDIR}/common/shell.sh

# Info
usage="mocs/scripts test suite execution script.
Usage:  scripts/test.sh [option] [option] ...
Options:
        -c, --coverage            Measure statement coverage.
        -d, --debug               Enable debug mode.
        -f, --filter <filter>     Only run tests containing <filter> text.
        -h, --help                Display this help and exit.
        -i, --input <file>        Only check one file.
        -m, --manifest <file>     Load a different MANIFEST file.
        -u, --function <function> Only check one function.

The program will read the file MANIFEST located in the folder where you execute
the program. The MANIFEST must contain data on where to find the code that shall
be tested and the test code. This is done by setting the following variables in
the MANIFEST:
        TEST_OBJECT='<path>'      The path to the code under test.
        TEST_SUITE='<path>'       The path to the test cases.

NOTE: Output will be sent to stdout and if coverage is used, a .cov file will be
placed next to each file under test and coverage will be reported on stdout."

#Check arguments
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -c|--coverage)
      DO_COV=1
      printf "${CYAN}"
      printf "Measuring statement coverage.\n"
      shift
      ;;
    -d|--debug)
      DEBUG=1
      printf "${CYAN}"
      printf "Running in debug mode.\n"
      shift
      ;;
    -f|--filter)
      FILTER="$2"
      printf "Filtering tests on '${FILTER}'.\n"
      shift
      shift
    ;;
    -h|--help)
      printf "${usage}\n"
      exit 0
      ;;
    -i|--input)
      SINGLE_FILE="$2"
      printf "Only testing one file: '${SINGLE_FILE}'.\n"
      shift
      shift
    ;;
    -m|--manifest)
      MANIFEST="$2"
      shift
      shift
    ;;
    -u|--function)
      SINGLE_FUNC="$2"
      printf "Only testing one function: '${SINGLE_FUNC}'.\n"
      shift
      shift
    ;;
    *)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      exit
  esac
done

# Sanity checks
printf "${MAGENTA}"
message "Executing sanity checks!"
printf "${NC}"
text="${cyan}Checking that we do not execute in self..."
printf "     $text"
if [[ "${TARGET}" == "${SRCDIR}" ]]; then
  printf "\r"
  _exit_cmd "This script cannot be executed in this folder." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking that we have a ${MANIFEST}..."
printf "     $text"
if ! [[ -f ${MANIFEST} ]]; then
  printf "\r"
  _exit_cmd "Could not find MANIFEST in ${TARGET}." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
source ${MANIFEST}
text="${cyan}Checking that we have code to test: ${TEST_OBJECT}..."
printf "     $text"
if ! [[ -d ${TEST_OBJECT} ]]; then
  printf "\r"
  _exit_cmd "Could not find ${TEST_OBJECT} folder." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking that we have test cases: ${TEST_SUITE}..."
printf "     $text"
if ! [[ -d ${TEST_SUITE} ]]; then
  printf "\r"
  _exit_cmd "Could not find ${TEST_SUITE} folder." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking that 'bats' is installed..."
printf "     $text"
if ! [[ $(which bats) ]]; then
  printf "\r"
  _exit_cmd "Could not find 'bats' command." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking 'bats' version is at least 1.2.0..."
printf "     $text"
batsVersion=$(bats --version)
re_bats='1.2'
if ! [[ $batsVersion =~ $re_bats ]]; then
  printf "\r"
  _exit_cmd "'bats' must be version 1.2.0 or higher." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
printf "${NC}"
text="${cyan}Checking 'sed' version..."
printf "     $text"
sed_is_gnu=$(sed --version 2> /dev/null | grep "GNU" ) || printf ""
if [[ ${#sed_is_gnu} -gt 0 ]]; then
  sed_cmd="sed -i''"
  printf "GNU"
else
  sed_cmd="sed -i ''"
  printf "not GNU"
fi
printf "\r${GREEN}OK   $text\n${NC}"

# Validate test suite files
printf "${MAGENTA}"
message "Validating test suite!"
printf "${NC}"
START=$(date +%s.%N | tr 'N' '0')

IFS=$'\r\n' FOUND_TESTS=($(grep -R "@test" ${TEST_SUITE}))
unset IFS

COUNT_WARN=0
COUNT_TOT=0
declare -A FILE_LIST
declare -A FILE_LIST_NOT_FOUND
ProgressBar 0 ${#FOUND_TESTS[@]}
for foundTest in "${FOUND_TESTS[@]}"; do
  COUNT_TOT=$(($COUNT_TOT+1))
  foundTest_File=$(echo "${foundTest}" | awk -F':' '{print $1}')
  foundTest_Info=$(echo "${foundTest}" | awk -F'"' '{print $2}')
  # Info should be of format: <filename>::<function>::<description>
  re_info="[a-zA-Z0-9_-]+::[a-zA-Z0-9_-]{0,}::(.*)+"
  if ! [[ "${foundTest_Info}" =~ $re_info ]]; then
    COUNT_WARN=$(($COUNT_WARN+1))
    printf "\r${BLUE}"
    message "${YELLOW}WARNING:${BLUE} ${foundTest_File} has a test
    (${foundTest_Info}) that does not match the required format:
    <filename>::<function>::<description>" "${BLUE}"
    printf "${NC}"
  fi
  # Check that file under test can be found
  foundTest_FileUnderTest=$(echo "${foundTest_Info}" | awk -F'::' '{print $1}')
  FILE_LIST["$foundTest_FileUnderTest"]=1
  findResult=$(find ${TEST_OBJECT} -name ${foundTest_FileUnderTest}'.*')
  if [[ ${#findResult} -eq 0 ]]; then
    FILE_LIST_NOT_FOUND["$foundTest_FileUnderTest"]=1
    printf "\r${BLUE}"
    message "${YELLOW}WARNING:${BLUE} The file '${foundTest_FileUnderTest}' was not found." "${BLUE}"
    printf "${NC}"
  fi
  ProgressBar $COUNT_TOT ${#FOUND_TESTS[@]}
done
printf "\n"
if [[ $COUNT_WARN -ne 0 ]]; then
  printf "${YELLOW}"
  message "${RED}NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE!${YELLOW} §
  There are ${RED}${COUNT_WARN}${YELLOW} test cases out of ${BLUE}${COUNT_TOT}${YELLOW}
  that do not match the required format:
  <filename>::<function>::<description>" "${YELLOW}"
  printf "${NC}"
fi
if [[ ${#FILE_LIST_NOT_FOUND[@]} -ne 0 ]]; then
  printf "${YELLOW}"
  message "${RED}NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE! NOTE!${YELLOW} §
  There are ${RED}${#FILE_LIST_NOT_FOUND[@]}${YELLOW} files under test out of ${BLUE}${#FILE_LIST[@]}${YELLOW}
  that cannot be found." "${YELLOW}"
  printf "${NC}"
fi
STOP=$(date +%s.%N | tr 'N' '0')
DIFF=$(subtract "$STOP" "$START")
TIME=$(format_time "${DIFF}")
printf "${CYAN}"
printf "Done validating in ${TIME}.\n\n"
printf "${NC}"

# Prepare bats command
test_cmd="bats -rp"
# Add filter regexps
addFilter=0
re_filter=''
if ! [ -z ${SINGLE_FILE+x} ]; then
  re_filter="^${SINGLE_FILE%.*}::"
  addFilter=1
else
  re_filter="^[a-zA-Z0-9_-]+::"
fi
if ! [ -z ${SINGLE_FUNC+x} ]; then
  re_filter+="${SINGLE_FUNC}::"
  addFilter=1
else
  re_filter+="[a-zA-Z0-9_-]{0,}::"
fi
if ! [ -z ${FILTER+x} ]; then
  re_filter+=".*${FILTER}(.*)+$"
  addFilter=1
else
  re_filter+="(.*)+$"
fi
# Add filter if there is one
if [[ ${addFilter} -ne 0 ]]; then
  test_cmd="$test_cmd -f '${re_filter}'"
fi

# Execute test suite
# Check if coverage or not
START=$(date +%s.%N | tr 'N' '0')
if ! [[ -z $DO_COV ]]; then
  covScratch="covScratch"
  printf "${MAGENTA}"
  message "Testing ${blue}with${MAGENTA} coverage!"
  printf "${NC}"

  # Cleanup
  text="${cyan}Cleaning previous data..."
  printf "     $text"
  printf "${NC}"
  rm -fr ${covScratch}
  printf "\r${GREEN}OK   $text\n${NC}"

  # Copying all files to covScratch
  text="${cyan}Copying files to ${covScratch}..."
  printf "     $text"
  printf "${NC}"
  mkdir -p ${covScratch}/src
  mkdir -p ${covScratch}/test
  cp -R ${TEST_OBJECT}/* ${covScratch}/src/
  cp -R ${TEST_SUITE}/* ${covScratch}/test/
  printf "\r${GREEN}OK   $text\n${NC}"

  # Instrument all files
  text="${cyan}Instrumenting scripts"
  printf "     $text"
  printf "${NC}"
  for filename in $(find "${covScratch}/src" -name '*.sh'); do
    ${SRCDIR}/coverage/instrument.sh ${filename}
    chmod +x ${filename}
    printf "${magenta}.${NC}"
  done
  printf "\r${GREEN}OK   $text\n${NC}"

  # Copy test suite and add instrumented files to tests.
  text="${cyan}Instrumenting tests"
  printf "     $text"
  printf "${NC}"
  testFiles=($(grep -R '${TEST_OBJECT}' "${covScratch}/test" | awk -F':' '{print $1}'))
  for filename in "${testFiles[@]}"; do
    ${sed_cmd} 's#${TEST_OBJECT}#'${covScratch}'/src/#g' "${filename}"
    printf "${magenta}.${NC}"
  done
  printf "\r${GREEN}OK   $text\n${NC}"

  # Execute instrumented test suite
  text="${cyan}Executing tests..."
  printf "     $text"
  printf "${NC}"
  eval ${test_cmd} ${covScratch}/test

  # Calculate and report
  source ${SRCDIR}/coverage/functions.sh
  text="${cyan}Calculating reports"
  printf "     $text"
  printf "${NC}"

  # Find files depth first to sort nicely for printing in the end.
  file_list=($(find "${covScratch}/src" -name '*.sh' | sort))
  file_list_sorted=($(depth_sort file_list[@]))

  for filename in "${file_list_sorted[@]}"; do
    # Check for excluded
    shortFilename=$(echo "$filename" | awk -F${covScratch}/src/ '{print $2}')
    if ! [[ -n "${EXCLUDE_FROM_COVERAGE[$shortFilename]}" ]]; then
      ${SRCDIR}/coverage/coverage.sh ${filename} ${covScratch}
    fi
    printf "${magenta}.${NC}"
  done
  printf "\r${GREEN}OK   $text\n${NC}"

  # Sum all reports
  report=""
  subHit=0
  subLoc=0
  totHit=0
  totLoc=0
  while read -r p || [ -n "$p" ]; do
    # Get info
    info=($p)
    currentFolder="${info[0]%/*}"
    # Check if we should make a partial sum
    if ! [[ -z $lastFolder ]]; then
      if [[ "${currentFolder}" != "${lastFolder}" ]]; then
        report="${report}SubTotal##10000\n"
        subCov=$(percentage ${subHit} ${subLoc})
        report="$report${lastFolder}##${subCov}\n"
        report="$report\n"
        subHit=0
        subLoc=0
      fi
    fi
    lastFolder="${currentFolder}"
    # Now to the real row
    subHit=$(( subHit+=${info[1]} ))
    subLoc=$(( subLoc+=${info[2]} ))
    totHit=$(( totHit+=${info[1]} ))
    totLoc=$(( totLoc+=${info[2]} ))
    cov=$(percentage ${info[1]} ${info[2]})
    report="$report${info[0]}##${cov}\n"
    if [[ ${#info[0]} -gt $colWidth ]]; then
      colWidth=${#info[0]}
    fi
  done < "${covScratch}/all.cov"
  # Add the final SubTotal
  report="${report}SubTotal##10000\n"
  subCov=$(percentage ${subHit} ${subLoc})
  report="$report${lastFolder}##${subCov}\n"
  report="$report\n"

  # Now add a total too
  cov=$(percentage ${totHit} ${totLoc})
  report="${report}\nTotal##10001\n"
  report="${report}Everything##${cov}\n"
  coverage_table $colWidth "${report}"
else
  printf "${MAGENTA}"
  message "Testing!"
  printf "${NC}"
  eval ${test_cmd} ${TEST_SUITE}
fi
STOP=$(date +%s.%N | tr 'N' '0')
DIFF=$(subtract "$STOP" "$START")
TIME=$(format_time "${DIFF}")
printf "${CYAN}"
printf "Done testing in ${TIME}.\n\n"

#Exit
printf "${MAGENTA}"
message "All done!"
printf "${NC}"
