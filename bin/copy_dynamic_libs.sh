#!/usr/bin/env bash

# This script uses ldd to find all dynamic libs and copies them to the supplied
# folder.

# Always die on error
set -e

# Check for DEBUG
if ! [ -z ${DEBUG+x} ]; then
  set -x
fi

# Setup
source shell.sh

function _help() {
  # No debug here
  set +x
  printf "${RED}"
  printf "You must supply exactly two arguments.\n"
  printf "${cyan}"
  printf "First must be an executable file.\n"
  printf "Second must be a folder.\n"
  printf "${NC}"
  exit 1
}

function get_filename() {
  # Use regexp to extract filename
  [[ "${1}" =~ (/([0-9A-Za-z/_-]*[+]*[.]*)*) ]] && printf "${BASH_REMATCH[1]}"
}

#Check arguments
if [ "$#" -ne 2 ]; then
  _help
fi
if [ -f "$1" ]; then
  FILE="$1"
else
  printf "${cyan}"
  printf "${1}${red} is not a file.\n"
  printf "${NC}"
  _help
fi
if [ -d "$2" ]; then
  FOLDER="$2"
else
  printf "${cyan}"
  printf "${2}${red} is not a folder.\n"
  printf "${NC}"
  _help
fi

# Find files
printf "${MAGENTA}"
message "Finding all dynamically linked libraries for ${cyan}${FILE}${MAGENTA} and copying them to ${cyan}${FOLDER}${MAGENTA}." "${MAGENTA}"
printf "${NC}"

# Check if supplied file is a link, if so, resolve it.
if [ -L "${FILE}" ]; then
  FILE=$(readlink -f "${FILE}")
fi

# Split into array by newline
IFS=$'\r\n' libraries=($(ldd "${FILE}"))
unset IFS
# Loop over each line
for lib in "${libraries[@]}"; do
  printf "Parsing ${cyan}${lib}${NC}"
  if [[ "${lib}" == *"/"* ]]; then
    file=$(get_filename "${lib}")
    dir="${file%/*}"
    mkdir -p ${FOLDER}${dir}
    # Check if file exists first. No overwrites allowed! This is because it
    # might cause extremely difficult debug situations when the container image
    # is finished. Better to inform the user of the problem during build time.
    if [[ -e "${FOLDER}${file}" ]]; then
      # Check if it's the same file. Then it's still ok.
      cmp --silent "${FOLDER}${file}" ${file}
      if [[ $? -ne 0 ]]; then
        printf "\r${RED}Fail   ${cyan}${lib} ${red}File already exists!\n"
        exit 2
      fi
    fi
    cp -L "${file}" "${FOLDER}${dir}"
    printf "\rDone   ${cyan}${lib}${NC} $file => ${FOLDER}${dir}"
  else
    printf "\r${YELLOW}Skipped"
  fi
  printf "\n"
done
