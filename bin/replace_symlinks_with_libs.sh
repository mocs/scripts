#! /bin/bash

# This script takes a root folder--containing library files and replaces
# the occurences that uses symlinks to be regular files. It will move the
# target of the symlink to the name of the symlink. It will do this
# recursively through all the child folders. It will be done in two steps,
# because some targets have multiple symlinks pointing to it. First step
# copies, the last step deletes the original targets.

# Always die on error
set -e

# Check for DEBUG
if ! [ -z ${DEBUG+x} ]; then
  set -x
fi

# Setup
source shell.sh

function _help() {
  # No debug here
  set +x
  printf "${RED}"
  printf "You must supply exactly one argument.\n"
  printf "${cyan}"
  printf "The argument has to be the absolute path to the root folder of library files. For example '/usr/lib64'.\n"
  printf "${NC}"
  exit 1
}

#Check arguments
if [ "$#" -ne 1 ]; then
  _help
fi
if [ "${1:0:1}" != "/" ]; then
  printf "${cyan}"
  printf "${red} You must provide an absolute path.\n"
  printf "${NC}"
  exit 1
fi
if [ -d "$1" ]; then
  LIB_ROOT_FOLDER="$1"
else
  printf "${cyan}"
  printf "${1}${red} is not a folder.\n"
  printf "${NC}"
  _help
fi

# Find files
printf "\n${MAGENTA}"
message "Replacing symlinks with their targets recursively in ${cyan}${LIB_ROOT_FOLDER}${MAGENTA}" "${MAGENTA}"
printf "${NC}\n"

function get_filename() {
  # Use regexp to extract filename
  [[ "${1}" =~ (/([0-9A-Za-z/_-]*[+]*[.]*)*) ]] && printf "${BASH_REMATCH[1]}"
}

# Get all symlink library files and split into array by newline
IFS=$'\r\n' symlinks_to_replace=($(find "${LIB_ROOT_FOLDER}" -type l -name "*.so*"))
unset IFS
COUNT=0
TOT=${#symlinks_to_replace[@]}

# Create target array
symlink_targets=()

# Loop over each line
if [[ -z $DEBUG ]]; then
  ProgressBar $COUNT $TOT
fi
for symlink in "${symlinks_to_replace[@]}"; do
  # Get the target
  symlink_tgt=$(readlink -f "${symlink}")
  # If symlink target is in LIB_ROOT_FOLDER hierarchy:
  if [[ "$symlink_tgt" == "${LIB_ROOT_FOLDER}"* ]]; then
    # Add target to target_array
    symlink_targets+=("${symlink_tgt}")
    # Replace symlink with target by copying
    cp --remove-destination "${symlink_tgt}" "${symlink}"
  fi
  COUNT=$(($COUNT+1))
  if [[ -z $DEBUG ]]; then
    ProgressBar $COUNT $TOT
  fi
done
printf "${NC}\n"

printf "\n${MAGENTA}"
message "Removing the original targets. . . ${MAGENTA}"
printf "${NC}\n"

COUNT=0
TOT=${#symlink_targets[@]}

# Loop over each line
if [[ -z $DEBUG ]]; then
  ProgressBar $COUNT $TOT
fi
# Delete all original targets
for symlink_target in "${symlink_targets[@]}"; do
  rm -f ${symlink_target}
  COUNT=$(($COUNT+1))
  if [[ -z $DEBUG ]]; then
    ProgressBar $COUNT $TOT
  fi
done
printf "${NC}\n"
