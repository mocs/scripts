#!/usr/bin/env bash

# Use this file find docker tags

# Always die on error
set -e

# Check for DEBUG
if ! [ -z ${DEBUG+x} ]; then
  set -x
fi

# Get the folder name of _this_ script
SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

# Get common stuff
source ${SRCDIR}/../common/shell.sh

# Info
usage="mocs/scripts docker tag validation script.
Usage:  scripts/bin/docker_tags.sh [option] [option] <image>...
Options:
        -a, --auth <token>        Authorization token.
        -h, --help                Display this help and exit.
        -r, --registry <registry> Choose registry.
                                  Default = registry.hub.docker.com
        -t, --tag <tag>           Verify if <tag> exists. 'TAG_FOUND' will be
                                  printed if it does. 'TAG_NOT_FOUND' will be
                                  printed if it does not exist.
Arguments:
        <image>                   Name of the image to look for.

The program will list all tags of the <image>. Which registry to use can be set.
If a tag is supplied, no listing will occur but the exit code will indicate if
the tag already exists or not.
"

registry="registry.hub.docker.com"

#Check arguments
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -a|--auth)
      ERR="Bad argument --auth"
      AUTH_TOKEN="$2"
      shift
      shift
      ;;
    -h|--help)
      printf "${NC}${usage}\n"
      exit 0
      ;;
    -r|--registry)
      ERR="Bad argument --registry"
      registry="$2"
      shift
      shift
      ;;
    -t|--tag)
      ERR="Bad argument --tag"
      tag="$2"
      shift
      shift
      ;;
    -*)
      # No debug here
      set +x
      ERR="Unknown command given. (${1})"
      printf "${NC}${usage}\n"
      exit 2
      ;;
    *)
      if [[ -z ${image} ]]; then
        ERR="Bad argument <image>"
        image=${1}
        shift
      else
        # No debug here
        set +x
        ERR="Unknown command given. (${1})"
        printf "${NC}${usage}\n"
        exit 2
      fi
      ;;
  esac
done
ERR="Unknown error."
if [[ -z ${image} ]]; then
  # No debug here
  set +x
  ERR="You must supply an image."
  exit 2
fi
# Create the url to get all tags
url="https://${registry}/v2/${image}/tags/list"
# Try to get it
ERR="curl error"
if [[ -z ${AUTH_TOKEN} ]]; then
  response=$(curl -sLi -H "Accept:application/vnd.docker.distribution.manifest.v2+json" "${url}")
else
  response=$(curl -sLi -H "Authorization: Basic ${AUTH_TOKEN}" -H "Accept:application/vnd.docker.distribution.manifest.v2+json" "${url}")
  header="Authorization: Basic ${AUTH_TOKEN}"
fi
httpResponseCode=$(echo "${response}" | head -n 1 | cut -d$' ' -f2)
#header=""
# Check response
if [[ "${httpResponseCode}" == "404" ]]; then
  printf "${RED}${image} ${BLUE}was not found at ${RED}${url}${NC}\n" 1>&2
  # Exit with code 0 since it is ok to push.
  # NOTE: Will fail checks! Is it really ok to push on 404?
  ERR="404. Could not access server."
  echo "TAG_NOT_FOUND"
  exit 0
elif [[ "${httpResponseCode}" == "401" ]]; then
  # e.g. Www-Authenticate: Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:library/debian:pull"
  # to: https://auth.docker.io/token?service=registry.docker.io&scope=repository:library/debian:pull
  # e.g. www-authenticate: Bearer realm="https://r.j3ss.co/auth",service="Docker registry",scope="repository:reg:pull"
  # to https://r.j3ss.co/auth?service=Docker%20registry&scope=repository:reg:pull'
  # URL encode any blanks such as service="Docker registry"
  # Remove all remaining spaces at the end to avoid quoting issues
  authUrl=$(echo "${response}" | grep -i www-Authenticate \
            | sed 's|.*Bearer realm="\(.*\)"|\1|' | sed 's|",service|?service|' | sed 's|",scope|\&scope|' | tr -d '"' \
            | sed 's| |%20|' |  tr -d '[:space:]' )
  token="$(curl -sSL "${authUrl}")"
  token="$(echo "${token}" | awk -F'token' '{print $2}' | awk -F':"' '{print $2}' | awk -F'"' '{print $1}')"
  header="Authorization: Bearer ${token}"
elif [[ "${httpResponseCode}" == "200" ]]; then
  response=$(echo ${response} | awk 'END{print}')
else
  ERR="Request failed. Response: ${response}"
  exit 1
fi

# If trying to simplify this into a variable "-H $header" you enter quoting hell
if [[ -z "${header}" ]]; then
    response=$(curl -sL -H "Accept:application/vnd.docker.distribution.manifest.v2+json" "${url}")
else
    response=$(curl -sL -H "${header}" -H "Accept:application/vnd.docker.distribution.manifest.v2+json" "${url}")
fi
# Print it for debugging purposes
if ! [ -z ${DEBUG+x} ]; then
  echo "RESPONSE = ${response[@]}"
fi
if [[ $response == *"UNAUTHORIZED"* ]]; then
  ERR="UNAUTHORIZED"
  exit 2
fi

# Get the tags only
json=$(echo "${response}" | awk -F'tags' '{print $2}' | awk -F'[' '{print $2}' | awk -F']' '{print $1}')

IFS=','
tags=($(echo "${json}" | tr -d '"'))
unset IFS

# Present them
ERR="Unknown error."
if [[ -z ${tag} ]]; then
  printf "${CYAN}"
  message "${CYAN}Found ${MAGENTA}${#tags[@]}${CYAN} tags!" "${CYAN}"
  printf "${NC}"
  for t in ${!tags[@]}; do
    echo "$(($t+1)) = ${tags[$t]}"
  done
else
  for t in ${!tags[@]}; do
    if [[ "${tags[$t]}" == "${tag}" ]]; then
      echo "TAG_FOUND"
      exit 0
    fi
  done
  echo "TAG_NOT_FOUND"
  exit 0
fi
