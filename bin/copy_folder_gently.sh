#!/usr/bin/env bash

# This script copies a folder i a "gentle" manner. This means that is a file
# already exists at the destination, a checksum is done on both files. If the
# checksum matches, the script ignores the file and continues. If the checksum
# differs, an error is thrown.

# Always die on error
set -e

# Check for DEBUG
if ! [ -z ${DEBUG+x} ]; then
  set -x
fi

# Setup
source shell.sh

function _help() {
  # No debug here
  set +x
  printf "${RED}"
  printf "You must supply exactly two arguments.\n"
  printf "${cyan}"
  printf "First must be a folder.\n"
  printf "Second must be a folder.\n"
  printf "${NC}"
  exit 1
}

#Check arguments
if [ "$#" -ne 2 ]; then
  _help
fi
if [ -d "$1" ]; then
  SRC_FOLDER="$1"
else
  printf "${cyan}"
  printf "${1}${red} is not a folder.\n"
  printf "${NC}"
  _help
fi
if [ -d "$2" ]; then
  TARGET_FOLDER="$2"
else
  printf "${cyan}"
  printf "${2}${red} is not a folder.\n"
  printf "${NC}"
  _help
fi

# Find files
printf "${MAGENTA}"
message "Copying ${cyan}${SRC_FOLDER}${MAGENTA} to ${cyan}${TARGET_FOLDER}${MAGENTA}.
§ Gently. § If a file already exists but with a different checksum, an error will be thrown." "${MAGENTA}"
printf "${NC}"

function get_filename() {
  # Use regexp to extract filename
  [[ "${1}" =~ (/([0-9A-Za-z/_-]*[+]*[.]*)*) ]] && printf "${BASH_REMATCH[1]}"
}

# Split into array by newline
IFS=$'\r\n' files_to_copy=($(find "${SRC_FOLDER}"))
unset IFS
COUNT=0
TOT=${#files_to_copy[@]}
# Loop over each line
if [[ -z $DEBUG ]]; then
  ProgressBar $COUNT $TOT
fi
for file in "${files_to_copy[@]}"; do
  # Get the filename
  stripped=$([[ $file =~ ${SRC_FOLDER}(.*) ]] && echo "${BASH_REMATCH[1]}")
  if [[ ${#stripped} -gt 0 ]]; then
    if [[ -d ${file} ]]; then
      # Folder. Just create.
      if ! [[ -z $DEBUG ]]; then
        printf "Creating folder ${cyan}${stripped}${NC} in ${cyan}${TARGET_FOLDER}${NC}"
      fi
      mkdir -p "${TARGET_FOLDER}/${stripped}"
      if ! [[ -z $DEBUG ]]; then
        printf "\n"
      fi
    else
      if ! [[ -z $DEBUG ]]; then
        printf "Copying ${cyan}${stripped}${NC} from ${cyan}${SRC_FOLDER}${NC} to ${cyan}${TARGET_FOLDER}${NC}"
      fi
      if [[ -f "${TARGET_FOLDER}/${stripped}" ]]; then
        # File exists. Compare checksums.
        if test -h "$file"; then
           # If symlink: compare targets of symlinks
          src_symlink_tgt=$(get_filename "${file}")
          tgt_symlink_tgt=$(get_filename "${TARGET_FOLDER}/${stripped}")
          hash_src=($(sha256sum "${src_symlink_tgt}"))
          hash_tgt=($(sha256sum "${tgt_symlink_tgt}"))
        else
          hash_src=($(sha256sum "${file}"))
          hash_tgt=($(sha256sum "${TARGET_FOLDER}/${stripped}"))
        fi
        if [[ "${hash_src[0]}" != "${hash_tgt[0]}" ]]; then
          printf "\r       \r"
          _exit_cmd "${BLUE}The file ${RED}${stripped}${BLUE} already exists at ${cyan}${TARGET_FOLDER}${BLUE}. Content ${RED}do not${BLUE} match!" 3
        fi
      else
        # Copy the file
        if test -h "$file"; then
          # If symlink
          cp -P "${file}" "${TARGET_FOLDER}/${stripped}"
        elif [[ "$file" == *'.uuid' ]]; then
          # If uuid file: skip
          printf "\rSkipping: ${file}\n"
        elif [[ $(get_filename "${file}") == *'/dev/null' ]]; then
          # If /dev/null: skip
          printf "\rSkipping: ${file}\n"
        elif [[ $(get_filename "${file}") == *'.log' ]]; then
          # If logfile: skip
          printf "\rSkipping: ${file}\n"
        # elif [[ $(get_filename "${file}") == *'.cache' ]]; then
        #   # If cachefile: skip
        #   printf "\rSkipping: ${file}\n"
        # elif [[ $(get_filename "${file}") == *'/var/log/'*  && -f "${file}" ]]; then
        #   # If it is a file under /var/log: skip
        #   printf "\rSkipping: ${file}\n"
        # elif [[ $(get_filename "${file}") == *'/usr/share/mime/globs'* ]]; then
        #   # If /usr/share/mime/globs: skip
        #   printf "\rSkipping: ${file}\n"
        # elif [[ $(get_filename "${file}") == *'/var/lib/rpm/'* ]]; then
        #   # If /var/lib/rpm/*: skip
        #   printf "\rSkipping: ${file}\n"
        # elif [[ $(get_filename "${file}") == *'/var/lib/yum/'* ]]; then
        #   # If /var/lib/yum/*: skip
        #   printf "\rSkipping: ${file}\n"
        # elif [[ $(get_filename "${file}") == *'/var/cache/'* ]]; then
        #   # If /var/cache*: skip
        #   printf "\rSkipping: ${file}\n"
        else
          cp -L "${file}" "${TARGET_FOLDER}/${stripped}"
        fi
      fi
      if ! [[ -z $DEBUG ]]; then
        printf "\n"
      fi
    fi
  fi
  COUNT=$(($COUNT+1))
  if [[ -z $DEBUG ]]; then
    ProgressBar $COUNT $TOT
  fi
done
printf "${NC}\n"
