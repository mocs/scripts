#!/usr/bin/env bash

# Use this file to start the build.

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

# Get common stuff
source ${SRCDIR}/common/shell.sh

#Check arguments
CLEAN=0
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -d|--debug)
      DEBUG=1
      printf "${CYAN}"
      printf "Running in debug mode.\n"
      shift
      ;;
    -m|--manifest)
      MANIFEST="$2"
      shift
      shift
    ;;
    -n|--no-login)
      NOLOGIN=1
      shift
    ;;
    *)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      exit
  esac
done
printf "${CYAN}"
printf "Using manifest file: ${BLUE}${MANIFEST}${NC}\n"


# Sanity checks
printf "${MAGENTA}"
message "Executing sanity checks!"
printf "${NC}"
text="${cyan}Checking that we do not execute in self..."
printf "     $text"
if [[ "${TARGET}" == "${SRCDIR}" ]]; then
  printf "\r"
  _exit_cmd "This script cannot be executed in this folder." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking that we have a ${MANIFEST}..."
printf "     $text"
if ! [[ -f ${MANIFEST} ]]; then
  printf "\r"
  _exit_cmd "Could not find MANIFEST in ${TARGET}." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
source ${MANIFEST}
for i in "${!BUILDS[@]}"; do
  docker_file=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $1}')
  text="${cyan}Checking that we have a ${docker_src}/${docker_file}..."
  printf "     $text"
  if ! [[ -f ${docker_src}/${docker_file} ]]; then
    printf "\r"
    _exit_cmd "Could not find ${docker_src}/${docker_file}." 1
  else
    printf "\r${GREEN}OK   $text\n${NC}"
  fi
done


if [ -z ${NOLOGIN+x} ]; then
  printf "${MAGENTA}"
  message "Logging in"
  printf "${NC}"
  for i in "${!BUILDS[@]}"; do
    image_tag=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $2}')
    # Check if it already exists.
    registry=$(echo "${image_tag}" | awk -F'/' '{print $1}')
    docker login ${registry}
  done
fi

printf "${MAGENTA}"
message "Pushing"
printf "${NC}"
for i in "${!BUILDS[@]}"; do
  image_tag=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $2}')
  # Check if it already exists.
  registry=$(echo "${image_tag}" | awk -F'/' '{print $1}')
  image=$(echo "${image_tag}" | awk -F'/' '{$1="";print $0}' | sed -e 's/^[[:space:]]*//' | tr ' ' '/' | awk -F':' '{print $1}')
  tag=$(echo "${image_tag}" | awk -F':' '{print $2}')
  # Disable exit on error since exit codes are used for information.
  set +e
  result=$(${SRCDIR}/bin/docker_tags.sh -r ${registry} ${image} -t ${tag} -a ${AUTH_TOKEN})
  code=$?
  set -e
  if [[ "${code}" -eq "0" && "${tag}" != 'latest' ]]; then
    printf "${RED}${image_tag}${BLUE} already exists.\n"
    printf "${red}Not pushing!${NC}\n"
    exit 1
  elif [[ "${code}" -eq 1 || "${tag}" == 'latest' ]]; then
    printf "${cyan}Ok to push ${MAGENTA}${image_tag}${NC}\n"
    docker push ${image_tag}
  elif [[ "${code}" -gt 1 ]]; then
    printf "${RED}Error occured with code ${code}.\n"
    exit ${code}
  fi
done
