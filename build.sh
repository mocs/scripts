#!/usr/bin/env bash

# Use this file to start the build.

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

# Get common stuff
source ${SRCDIR}/common/shell.sh

#Check arguments
CLEAN=0
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -d|--debug)
      DEBUG=1
      progress='--progress plain'
      printf "${CYAN}"
      printf "Running in debug mode.\n"
      shift
      ;;
    -c|--clean)
      CLEAN=1
      printf "${CYAN}"
      printf "Will clean when done.\n"
      shift
      ;;
    -m|--manifest)
      MANIFEST="$2"
      shift
      shift
    ;;
    *)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      exit
  esac
done
printf "${CYAN}"
printf "Using manifest file: ${BLUE}${MANIFEST}${NC}\n"


# Sanity checks
printf "${MAGENTA}"
message "Executing sanity checks!"
printf "${NC}"
text="${cyan}Checking that we do not execute in self..."
printf "     $text"
if [[ "${TARGET}" == "${SRCDIR}" ]]; then
  printf "\r"
  _exit_cmd "This script cannot be executed in this folder." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking that we have a ${MANIFEST}..."
printf "     $text"
if ! [[ -f ${MANIFEST} ]]; then
  printf "\r"
  _exit_cmd "Could not find MANIFEST in ${TARGET}." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
source ${MANIFEST}
text="${cyan}Checking that we have a source '${docker_src}'..."
printf "     $text"
if ! [[ -d ${docker_src} ]]; then
  printf "\r"
  _exit_cmd "Could not find ${docker_src} folder." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking that we have a defined architecture..."
printf "     $text"
if [[ -z ${arch} ]]; then
  printf "\r"
  _exit_cmd 'Could not find the variable ${arch}.' 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
for i in "${!BUILDS[@]}"; do
  docker_file=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $1}')
  text="${cyan}Checking that we have a ${docker_src}/${docker_file}..."
  printf "     $text"
  if ! [[ -f ${docker_src}/${docker_file} ]]; then
    printf "\r"
    _exit_cmd "Could not find ${docker_src}/${docker_file}." 1
  else
    printf "\r${GREEN}OK   $text\n${NC}"
  fi
done


# Build the image(s)
for i in "${!BUILDS[@]}"; do
  docker_file=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $1}')
  image_tag=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $2}')
  build_args=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $3}')
  printf "${MAGENTA}"
  message "Building ${CYAN}${docker_file}${YELLOW} ==> ${CYAN}${image_tag}${MAGENTA}" "${MAGENTA}"
  printf "${NC}"
  START=$(date +%s.%N | tr 'N' '0')
  ${docker_cmd} build ${progress} ${build_args} -t "${image_tag}" -f "${docker_src}/${docker_file}" "${docker_src}"
  EC=$?
  STOP=$(date +%s.%N | tr 'N' '0')
  DIFF=$(subtract "$STOP" "$START")
  TIME=$(format_time "${DIFF}")
  printf "${CYAN}"
  printf "Done building in ${TIME}.\n\n"
  #Check for success
  check_success "${EC}"

  # Debug?
  if ! [ -z ${DEBUG+x} ]; then
    #Start docker container
    printf "${MAGENTA}"
    message "Starting ${CYAN}${image_tag}${MAGENTA}" "${MAGENTA}"
    printf "${NC}"
    START=$(date +%s.%N | tr 'N' '0')
    ${docker_cmd} run -it "${image_tag}"
    STOP=$(date +%s.%N | tr 'N' '0')
    DIFF=$(subtract "$STOP" "$START")
    TIME=$(format_time "${DIFF}")
    printf "${CYAN}"
    printf "Done debugging in ${TIME}.\n\n"
  fi
done

# Cleanup!
if [ "$CLEAN" == 1 ];then
  ${TARGET}/scripts/clean.sh
fi

#Exit
printf "${MAGENTA}"
message "All done!"
printf "${NC}"
