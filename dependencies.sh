#!/usr/bin/env bash

# Use this file to check/download all dependencies.

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "${BASH_SOURCE[0]%/*}" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

blob_target='src/blobs/'

# Get common stuff
source ${SRCDIR}/common/shell.sh

#Check arguments
CLEAN=0
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -d|--debug)
      DEBUG=1
      printf "${CYAN}"
      printf "Running in debug mode.\n"
      printf "${NC}"
      shift
      ;;
    -c|--clean)
      CLEAN=1
      printf "${CYAN}"
      printf "Will remove all downloaded dependencies and download again.\n"
      printf "${NC}"
      shift
      ;;
    -m|--manifest)
      MANIFEST="$2"
      shift
      shift
    ;;
    *)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      exit
  esac
done
printf "${CYAN}"
printf "Using manifest file: ${BLUE}${MANIFEST}${NC}\n"


# Sanity checks
printf "${MAGENTA}"
message "Executing sanity checks!"
printf "${NC}"
text="${cyan}Checking that we do not execute in self..."
printf "     $text"
if [[ "${TARGET}" == "${SRCDIR}" ]]; then
  printf "\r"
  _exit_cmd "This script cannot be executed in this folder." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
text="${cyan}Checking that we have a ${MANIFEST}..."
printf "     $text"
if ! [[ -f ${MANIFEST} ]]; then
  printf "\r"
  _exit_cmd "Could not find MANIFEST in ${TARGET}." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
source ${MANIFEST}
text="${cyan}Checking that we have a ${DEPENDENCIES_FILE}..."
printf "     $text"
if ! [[ -f ${DEPENDENCIES_FILE} ]]; then
  printf "\r"
  _exit_cmd "Could not find ${DEPENDENCIES_FILE}." 1
else
  printf "\r${GREEN}OK   $text\n${NC}"
fi
printf "${cyan}"
# Load dependencies
source ${DEPENDENCIES_FILE}
text="${cyan}Checking that we have a sane ${DEPENDENCIES_FILE}..."
printf "     $text"
re_web='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&\\(\)\*\+,;=.]+$'
re_sha='/\b([a-f0-9]{40})\b/'
re_folder='^/|(/[\w-]+)+$'
if [[ $((${#dependencies[@]} % 3)) -ne 0 ]]; then
  printf "\r"
  _exit_cmd "'dependencies' must be modulo three." 1
else
  for (( i = 0; i < $((${#dependencies[@]}/3)); i++ )); do
    # Check that first item is a valid web address
    if [[ "${bin_repo}${dependencies[$(($i*3))]}" =~ $re_web ]]; then
      printf "\r"
      _exit_cmd "${bin_repo}${dependencies[$(($i*3))]} is not a valid web address." 1
    fi
    # Check that second item is a valid sha1 sum
    if [[ "${dependencies[$(($i*3+1))]}" =~ $re_sha ]]; then
      printf "\r"
      _exit_cmd "${dependencies[$(($i*3+1))]} is not a valid sha1 sum." 1
    fi
    # Check that third item is a valid folder
    if [[ "${dependencies[$(($i*3+2))]}" =~ $re_folder ]]; then
      printf "\r"
      _exit_cmd "${dependencies[$(($i*3+2))]} is not a valid folder name." 1
    fi
  done
  printf "\r${GREEN}OK   $text\n${NC}"
fi
printf "${cyan}"


if [[ $CLEAN -eq 1 ]]; then
  printf "${MAGENTA}"
  message "Removing existing dependencies!"
  printf "${NC}"
  START=$(date +%s.%N | tr 'N' '0')
  # Set error message first since exit code will be trapped.
  ERR="Failed to remove existing dependencies from ${blob_target}"
  $(rm -fr "${blob_target}")
  unset ERR
  STOP=$(date +%s.%N | tr 'N' '0')
  DIFF=$(subtract "$STOP" "$START")
  TIME=$(format_time "${DIFF}")
  printf "${CYAN}"
  printf "Done cleaning dependencies in ${TIME}.\n"
fi

# Check and download all dependencies
printf "${MAGENTA}"
message "Checking dependencies!"
printf "${NC}"
START=$(date +%s.%N | tr 'N' '0')
for (( i = 0; i < $((${#dependencies[@]}/3)); i++ )); do
  printf "${YELLOW}Downloading${NC} ${bin_repo}${dependencies[$(($i*3))]}"
  folder_name="${blob_target}${dependencies[$(($i*3+2))]}"
  folder_name="${folder_name%/*}"
  mkdir -p "${folder_name}"
  # Set error message first since exit code will be trapped.
  ERR="Failed to download ${bin_repo}${dependencies[$(($i*3))]}"
  $(LC_ALL=en_US.UTF-8 wget -q "${bin_repo}${dependencies[$(($i*3))]}" -O "${blob_target}${dependencies[$(($i*3+2))]}")
  unset ERR
  printf "\r${YELLOW}Verifying${NC}   ${blob_target}${dependencies[$(($i*3+2))]}"
  # Set error message first since exit code will be trapped.
  ERR="Could not create sha1 checksum."
  sha=($(sha1sum ${blob_target}${dependencies[$(($i*3+2))]}))
  ERR="SHA1 checksum differs for ${blob_target}${dependencies[$(($i*3+2))]}"
  if [[ "${sha[0]}" != "${dependencies[$(($i*3+1))]}" ]]; then
    exit 1
  fi
  unset ERR
  printf "\r${GREEN}OK          ${bin_repo}${dependencies[$(($i*3))]}"
  printf "\n"
done
STOP=$(date +%s.%N | tr 'N' '0')
DIFF=$(subtract "$STOP" "$START")
TIME=$(format_time "${DIFF}")
printf "${CYAN}"
printf "Done checking dependencies in ${TIME}.\n"


#Exit
printf "${MAGENTA}"
message "All done!"
printf "${NC}"
